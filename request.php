<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>
        GET
    </title>
</head>
<body>
<div class="container">
    <h2>Request Form</h2>
    <form class="form-horizontal" role="form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2" for="text">Name</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" id="text" placeholder="Enter Name" name="name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email</label>
            <div class="col-sm-5">
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="password">Password</label>
            <div class="col-sm-5">
                <input type="password" class="form-control" id="password" placeholder="Enter email" name="password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default" value="submit">Submit</button>
            </div>
        </div>
    </form>
</div>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_REQUEST['name'];
    $email = $_REQUEST['email'];
    if (empty($name) || empty($email) ) {
        echo "Name or Email is empty. Please fill them";
    } else {
        echo "<hr><div><h1>"."You Entered"."<hr>".$name."(".$email.")"."</h1></div>";
    }
}
?>

</body>

</html>